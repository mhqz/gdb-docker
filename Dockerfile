FROM debian:buster as base
RUN apt-get update && apt-get install -y \
    build-essential \
    libasio-dev \
    gdbserver

FROM base
COPY . .
RUN ["./compile.sh"]
CMD ["./udp-server"]
