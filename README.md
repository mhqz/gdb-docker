#gdb-docker

## Build and start


```bash
docker build -t mhqz/udp-server:latest .
```

```bash
docker-compose up
```

## Test the udp service

```bash
nc -v -u 127.0.0.1 55000
```

## Start the gdbserver inside the container

Toggle `ptrace_scope` to avoid `Operation not permitted` errors when gdbserver
is trying to attach to PID 1 inside of the Docker container

```bash
# cat /proc/sys/kernel/yama/ptrace_scope
1
# echo 0 > /proc/sys/kernel/yama/ptrace_scope
# cat /proc/sys/kernel/yama/ptrace_scope
0
```

```bash
docker exec -it gdb-node-1 bash
gdbserver --attach 127.0.0.1:8070 1
```

## Connect GDB to the gdbserver instance

```bash
gdb -q -ex "set debuginfod enabled off" -ex "target remote 127.0.0.1:8070"
```
Load the symbol table

```bash
(gdb) load udp-server
```

Set a breakpoint

```bash
(gdb) br 23
```

Continue and reach the breakpoint by sending data via netcat

```bash
(gdb) continue
```
