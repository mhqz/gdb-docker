#!/bin/bash
source=udp-server.cpp

g++ ${source} \
    -g \
    -o ${source%.c*} \
    -I/usr/include/asio/ \
    -I/usr/lib/x86_64-linux-gnu/ \
    -lpthread \
    -std=c++11
